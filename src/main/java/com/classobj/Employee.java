package com.classobj;

public class Employee {

    private int id;
    private String name;
    private int phone_number;
    private String role;
    private String city;
    private int permanent;

    public Employee() {

    }

    public Employee(String role, String city, int permanent, String name, int phone_number, int id) {
        this.role = role;
        this.city = city;
        this.permanent = permanent;
        this.name = name;
        this.phone_number = phone_number;
        this.id = id;
    }


    @Override
    public String toString() {
        return "Employee{" +
                "role='" + role + '\'' +
                ", city='" + city + '\'' +
                ", permanent=" + permanent +
                ", name='" + name + '\'' +
                ", phone_number=" + phone_number +
                ", id=" + id +
                '}';
    }


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPermanent() {
        return permanent;
    }

    public void setPermanent(int permanent) {
        this.permanent = permanent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(int phone_number) {
        this.phone_number = phone_number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
