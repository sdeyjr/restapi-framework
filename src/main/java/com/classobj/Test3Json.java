package com.classobj;

import java.util.HashMap;

public class Test3Json {

    private String feature;
    private String timestamp;
    private int id;
    private HashMap<String, String> data;

    public Test3Json() {

    }

    public Test3Json(String feature, String timestamp, int id, HashMap<String, String> data) {
        this.feature = feature;
        this.timestamp = timestamp;
        this.id = id;
        this.data = data;
    }


    @Override
    public String toString() {
        return "JsonClass{" +
                "feature='" + feature + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", id=" + id +
                ", data=" + data +
                '}';
    }


    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HashMap<String, String> getData() {
        return data;
    }

    public void setData(HashMap<String, String> data) {
        this.data = data;
    }

}
