package com.methods;

import com.classobj.Employee;
import com.classobj.Test3Json;
import com.classobj.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.methods.DataBaseMethods.getMapFromDBOfSingleRecord;

public class JsonFileProcessing {

    @SneakyThrows
    public static JSONArray getJsonArray(String path) {
        JSONParser jsonParser = new JSONParser();
        JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(path));
        return jsonArray;
    }

    @SneakyThrows
    public void readSingleArray_SingleJson() {
        JSONArray jsonArray = getJsonArray("src/main/resources/SingleArray_SingleJson.json");
        System.out.println(jsonArray.toJSONString());

        JSONObject jsonObject = (JSONObject) jsonArray.get(0);
        System.out.println(jsonObject);
        System.out.println(jsonObject.get("data"));
    }

    @SneakyThrows
    public void readSingleArray_MultipleJson() {
        JSONArray jsonArray = getJsonArray("src/main/resources/SingleArray_MultipleJson.json");
        System.out.println(jsonArray.toJSONString());

        JSONObject jsonObject = (JSONObject) jsonArray.get(1);
        System.out.println(jsonObject);
        System.out.println(jsonObject.get("data"));
    }

    @SneakyThrows
    public void readSingleArray_JsonInMultipleJson() {
        JSONArray jsonArray = getJsonArray("src/main/resources/SingleArray_JsonInMultipleJson.json");
        System.out.println(jsonArray.toJSONString());

        JSONObject jsonObject = (JSONObject) jsonArray.get(1);
        System.out.println(jsonObject);
        JSONObject rohanDetail = (JSONObject) jsonObject.get("data");
        System.out.println(rohanDetail.get("name"));
    }


    @SneakyThrows
    public void readSingleArray_ArrayInMultipleJson() {
        JSONArray jsonArray = getJsonArray("src/main/resources/SingleArray_ArrayInMultipleJson.json");
        System.out.println(jsonArray.toJSONString());

        JSONObject jsonObject = (JSONObject) jsonArray.get(1);
        System.out.println(jsonObject);

        JSONArray data = (JSONArray) jsonObject.get("data");
        System.out.println(data);

        JSONObject naimDetail = (JSONObject) data.get(1);
        String name = naimDetail.get("name").toString();
    }

    public void printMyName() {
        String name = "";
        System.out.println(name);
    }

    @SneakyThrows
    public void readTestJson() {
        String env = System.getProperty("environment");
        System.out.println(env);

        JSONArray jsonArray = getJsonArray("src/main/resources/Test.json");
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
            String environment = jsonObject.get("env").toString();

            if (environment.equalsIgnoreCase(env)) {
                JSONObject data = (JSONObject) jsonObject.get("data");
                System.out.println(data.get("name"));
            }
        }
    }

    public void useClassToStoreJson() throws JsonProcessingException {
        JSONArray jsonArray = getJsonArray("src/main/resources/User.json");
        JSONObject jsonObject = (JSONObject) jsonArray.get(0);

        ObjectMapper objectMapper = new ObjectMapper();
        User user = objectMapper.readValue(jsonObject.toJSONString(), User.class);

        System.out.println(user.toString());
        System.out.println(user.getName());
        System.out.println(user.getSalary());
        System.out.println(user.getTeamName());
    }

    public void retrivalOfData() throws SQLException, IOException {
       Map<String, String> data = getMapFromDBOfSingleRecord("SELECT * FROM worldOfAutomation.employee");

        List<String> allValues = new ArrayList<>();
        List<String> allKeys = new ArrayList<>();
        for (String keys: data.keySet()) {
            allKeys.add(keys);
        }
        for (String values: data.values()){
            allValues.add(values);
        }

        JSONObject jsonObject = new JSONObject();

        jsonObject.put(allKeys.get(0),allValues.get(0));
        jsonObject.put(allKeys.get(1),allValues.get(1));
        jsonObject.put(allKeys.get(2),allValues.get(2));
        jsonObject.put(allKeys.get(3),allValues.get(3));
        jsonObject.put(allKeys.get(4),allValues.get(4));
        jsonObject.put(allKeys.get(5),allValues.get(5));

        JSONArray jsonArray = new JSONArray();
        jsonArray.add(jsonObject);
        try{
            FileWriter file = new FileWriter("src/main/resources/Testing.json");
            file.write(jsonArray.toJSONString());
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Inserted into the Testing.json: "+jsonObject);
    }


    public void storingJsonIntoAClassObject() throws JsonProcessingException {
        JSONArray jsonArray = getJsonArray("src/main/resources/Testing.json");
        JSONObject jsonObject = (JSONObject) jsonArray.get(0);

        ObjectMapper objectMapper = new ObjectMapper();
        Employee employee = objectMapper.readValue(jsonObject.toJSONString(),Employee.class);

        System.out.println(employee.getName());
        System.out.println(employee.getCity());
        System.out.println(employee.getPermanent());
        System.out.println(employee.getPhone_number());
        System.out.println(employee.getRole());
        System.out.println(employee.getId());

    }

    public void storingTest3IntoClassObject() throws JsonProcessingException {
        JSONArray jsonArray = getJsonArray("src/main/resources/Test3.json");

        JSONObject jsonObject = (JSONObject) jsonArray.get(0);

        ObjectMapper objectMapper = new ObjectMapper();
        Test3Json test3Json = objectMapper.readValue(jsonObject.toJSONString(), Test3Json.class);
        System.out.println(test3Json);
        System.out.println(test3Json.getData().get("name"));
        System.out.println(test3Json.getData().get("id"));
        System.out.println(test3Json.getFeature());
        System.out.println(test3Json.getId());
        System.out.println(test3Json.getTimestamp());
    }
}
