package com.methods;

import org.testng.annotations.DataProvider;

import java.util.Random;

public class DataProviderClass {

    @DataProvider
    public Object [][] dataForPhoneNumber (){
        Random random =new Random();
        return new Object[][]{{random.nextInt(1000)},{random.nextInt(1000)}};
    }

    @DataProvider
    public Object [][] negativeDataForPhoneNumber (){
        return new Object[][]{{1111111111},{111111111},{11111111},{-11111}};
    }
}
